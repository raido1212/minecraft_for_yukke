#pragma once

#include <vector>
#include "dxlib.h"

#include "BaseFunction.h"


class Mino : public BaseFunc{
private:
	enum class eMinoType {
		Wall,
		I,
		O,
		S,
		Z,
		J,
		L,
		T,
		Max
	};

	VECTOR pos;
	int    color;

public:

	int Update();
	int Rotate();
	int Move();
	int SetMino();
	int ReloadMino();

	int Draw();

};