#include "DxLib.h"
#include "Game.h"


const int WINDOW_SIZE_X = 1080;
const int WINDOW_SIZE_Y = 720;

// 1�s1����
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

    bool isError = 0;

    if (DxLib_Init() == -1) {
        return -1;
    }

    cGame game;

	while (!ScreenFlip() && !ProcessMessage() && !ClearDrawScreen() ) {


        game.Update();

	}

    DxLib_End();
	return 0;
}