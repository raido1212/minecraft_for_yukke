#pragma once

class BaseFunc {
protected:
	virtual int Init() = 0;
	virtual int Update() = 0;
	virtual int Draw() = 0;
};